#include <sourcemod>

#define DATA "1.0.0.1"
#define Author "Markus"

Handle ad1_timer = INVALID_HANDLE;
Handle ad2_timer = INVALID_HANDLE;
Handle ad3_timer = INVALID_HANDLE;
Handle ad4_timer = INVALID_HANDLE;

public Plugin:myinfo = { 
    name = "Norse/NRS",
	author = Author,
	description = "",
	version = DATA,
	url = "https://steamcommunity.com/profiles/76561198312713613/"
}

public OnClientPutInServer(client) {
    
	ad1_timer = CreateTimer(5.0, Ad1_TimerCallback, client);
}

public Action Ad1_TimerCallback(Handle:timer, any:client) 
{
    new String:Name[32];
	GetClientName(client, Name, sizeof(Name));

	PrintToChat(client, "\x04%s\x01 Welcome to Norse/NRS", Name);
	PrintToChat(client, "Join our Discord here:\x03 https://discord.gg/WswEs9 \x01");
	PrintToChat(client, "We hope you will enjoy your time on the server!");
	CloseHandle(ad1_timer);
	
	ad2_timer = CreateTimer(600.0, Ad2_TimerCallback, client);
	
	return Plugin_Handled;
}

public Action Ad2_TimerCallback(Handle timer, any client) {
    PrintToChat(client, "Do you want a knife or a skin just do \x04!ws\x01 and \x04!knife\x01");
	CloseHandle(ad2_timer);
	
	ad3_timer = CreateTimer(600.0, Ad3TimerCallback, client);
	
	return Plugin_Handled;
}

public Action Ad3TimerCallback(Handle timer, any client) {
   PrintToChat(client, "If you need an admin just use the !calladmin command and there will come an admin shortly!");
   CloseHandle(ad3_timer);
   
   ad4_timer = CreateTimer(600.0, Ad4TimerCallback, client);
   
   return Plugin_Handled;
}

public Action Ad4TimerCallback(Handle timer, any client) {
    PrintToChat(client, "Join our Discord here: \x03https://discord.gg/WswEs9 \x01");
	CloseHandle(ad4_timer);
	
	return Plugin_Handled;
}